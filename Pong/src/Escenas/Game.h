#ifndef GAME_H
#define GAME_H
#include "raylib.h"

#include "Global.h"
#include "MenuToGame.h"

#include "../Objetos/Player.h"
#include "../Objetos/Ball.h"
#include "../Objetos/PowerUp.h"



namespace PONGJB
{
	enum TIME
	{
		SECONDS, MINUTES, HOURS
	};
	class Game
	{
	public:
		const int screenWidth = 810;
		const int screenHeight = 450;

		Game();
		~Game();
		void init();
		void input();
		void update();
		void draw();
		void gameTime();

		void switchInGame();

		void setGameInT();
		void setPlayersColor();
		void setOwnGameStatus();
		void setPointsToWin();
		void setIsPVP(bool ispvp);

		int getPointsToWin();
		bool getIsPVP();

		Player p1;
		Player p2;
		Ball ball;
		Ball ball2;
		bool ball2Active;
		Rectangle Obstacle;
		bool ObstacleActive;
		PowerUp pUP;

		int actualPowerUp;
		Player winer;


		GameStage gamestatus;
	private:
		int pointsToWin;
		bool isPvp;
		bool inGame;
		bool pause;
		float seconds;
		int time[3];
		void powerUpEffect();
		Music music;
		bool musicOn;

	};
}
#endif