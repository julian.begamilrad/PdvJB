#include "MenuToGame.h"

namespace PONGJB
{
	 Color MenuToGame::_pc1 = BLUE;
	 Color MenuToGame::_pc2 = RED;
	 int MenuToGame::_pointsToWin = BASEPOINTSTOWIN;

	void MenuToGame::set_pc1(Color p1color)
	{
		_pc1 = p1color;
	}

	void MenuToGame::set_pc2(Color p2color)
	{
		_pc2 = p2color;
	}

	void MenuToGame::set_pointsToWin(int ptw)
	{
		_pointsToWin = ptw;
	}

	int MenuToGame::get_pointsToWin()
	{
		return _pointsToWin;
	}

	Color MenuToGame::get_pc1()
	{
		return _pc1;
	}

	Color MenuToGame::get_pc2()
	{
		return _pc2;
	}
	
}