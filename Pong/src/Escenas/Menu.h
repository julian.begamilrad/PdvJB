#ifndef MENU_H
#define MENU_H
#include "raylib.h"

#include "Global.h"
#include "MenuToGame.h"
namespace PONGJB
{
	static const int TEXTDISTANCEINX = 2;
	static const int PLAYTEXTDISTANCEINY = 6;
	static const int CONTROLESTEXTDISTANCEINY = 8;
	static const int CREDITSTEXTDISTANCEINY = 10;
	static const int EXITTEXTDISTANCEINY = 12;
	static const int DISTANCEDEFAULT = 30;

	class Menu
	{
		enum MenuOptions
		{
			Play, Controls, Credits, Exit
		};
		enum GameSelection
		{
			NONE, PVP, VSIA, PUNTOS, BACK
		};
		enum OriginalMovments
		{
			W, S, UP, DOWN
		};
		Color colores[6] = { BLUE, RED, YELLOW, GREEN, ORANGE, PURPLE };
	public:
		Menu();
		~Menu();
		void init();
		void input();
		void update();
		void draw();
		void setOwnGameStatus();
		int actualOption;
		bool isControlMenu;
		int pointsRequisite;
		GameStage gamestatus;
		GameSelection actualGameStatus;
		Color p1Color;
		Color p2Color;
	private:
		bool getIsControlMenu();
		bool pickingGame;
		void changeIsControlMenu();
		
		int Ip1Color;
		
		int Ip2Color;

		Sound select;
		Sound moveSelection;
		
	};
}
#endif