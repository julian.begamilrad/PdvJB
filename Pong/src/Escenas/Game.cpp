#include "Game.h"

namespace PONGJB
{
	Game::Game()
	{		
		ball2Active = true;
		Obstacle.height = 0;
		Obstacle.width = 0;
		Obstacle.x = 0;
		Obstacle.y = 0;
		ObstacleActive = false;		
		actualPowerUp = NONE;
		gamestatus = GAME;
		pointsToWin = BASEPOINTSTOWIN;
		 isPvp = false;
		 seconds= 0;
		 time[0] = 0;
		 time[1] = 0;
		 time[2] = 0;
		pause = false;
		inGame = true;
		musicOn = true;
		music = LoadMusicStream("res/GameMusic.mp3");
	}

	Game::~Game()
	{
		UnloadMusicStream(music);
		
	}

	void Game::init()
	{
		time[SECONDS] = 0;
		time[MINUTES] = 0;
		time[HOURS] = 0;
		actualPowerUp = NONE;
		p1.init(1);
		p2.init(2);
		winer = p1;
		ball.init();
		ball2Active = false;
		ball2.init();
		ObstacleActive = false;
		Obstacle.y = screenHeight / HALFDIVIDER - (HEIGHT / HALFDIVIDER);
		Obstacle.x = screenWidth / HALFDIVIDER - (WIDTH/ HALFDIVIDER);
		Obstacle.height = HEIGHT;
		Obstacle.width = WIDTH/QUARTERDIVIDER;
		seconds = 0;
		setPlayersColor();
		setPointsToWin();
		pUP.setPosition({ OFFSCREENPOS , OFFSCREENPOS });
		PlayMusicStream(music);
		SetMusicVolume(music,1);
		musicOn = true;
	}
	void Game::input()
	{
		if (IsKeyReleased(KEY_P))
		{
			pause = !pause;

			if (pause) PauseMusicStream(music);
			else if (musicOn) ResumeMusicStream(music);
		}
		if (IsKeyReleased(KEY_N))
		{
			if (musicOn)PauseMusicStream(music);
			else if (!pause)ResumeMusicStream(music);
			musicOn = !musicOn;
		}
		if (pause == false)
		{
			p1.input();
			if (isPvp == true)
			{
				p2.input();
			}
		}

		if (pause == true)
		{
			if (IsKeyReleased(KEY_M))
			{
				switchInGame();
				gamestatus = MENU;
				init();
				pause = false;
			}

			if (IsKeyReleased(KEY_R))
			{
				init();
				pause = false;
			}
		}

	}
	void Game::update()
	{
		UpdateMusicStream(music);
		gameTime();
		int puntoPara = 0;
		if (pause != true)
		{
			ball.move();
			if (ball2Active)
			{
				ball2.move();
				puntoPara = ball2.testLimits();
				if (puntoPara == 1)
				{
					if (p2.getShield())
					{
						p2.swapShield();
					}
					else
					{
						p1.addPoint();
						if (p2.getInvControls()) p2.swapInvControls();
						if (p1.getInvControls()) p1.swapInvControls();
						p1.setHeigh(HEIGHT);
						p2.setHeigh(HEIGHT);
						p1.setPSpeed(PLAYERSPEED);
						p2.setPSpeed(PLAYERSPEED);
						ball2Active = false;
						ObstacleActive = false;
					}
				}
				if (puntoPara == 2)
				{
					if (p1.getShield())
					{
						p1.swapShield();
					}
					else
					{
						p2.addPoint();
						if (p2.getInvControls()) p2.swapInvControls();
						if (p1.getInvControls()) p1.swapInvControls();
						p1.setHeigh(HEIGHT);
						p2.setHeigh(HEIGHT);
						p1.setPSpeed(PLAYERSPEED);
						p2.setPSpeed(PLAYERSPEED);
						ball2Active = false;
						ObstacleActive = false;
					}

				}
				ball2.checkColisionWihtPlayers(p1.getBody(), p2.getBody());
			}
			puntoPara = ball.testLimits();
			if (puntoPara == 1)
			{
				if (p2.getShield())
				{
					p2.swapShield();
				}
				else
				{
					p1.addPoint();
					if (p2.getInvControls()) p2.swapInvControls();
					if (p1.getInvControls()) p1.swapInvControls();
					p1.setHeigh(HEIGHT);
					p2.setHeigh(HEIGHT);
					p1.setPSpeed(PLAYERSPEED);
					p2.setPSpeed(PLAYERSPEED);
					ball2Active = false;
					ObstacleActive = false;
				}
			}
			if (puntoPara == 2)
			{
				if (p1.getShield())
				{
					p1.swapShield();
				}
				else
				{
					p2.addPoint();
					if (p2.getInvControls()) p2.swapInvControls();
					if (p1.getInvControls()) p1.swapInvControls();
					p1.setHeigh(HEIGHT);
					p2.setHeigh(HEIGHT);
					ObstacleActive = false;
					p1.setPSpeed(PLAYERSPEED);
					p2.setPSpeed(PLAYERSPEED);
					ball2Active = false;
				}

			}
			ball.checkColisionWihtPlayers(p1.getBody(), p2.getBody());

			if (ObstacleActive)
			{
				if (CheckCollisionCircleRec(ball.ballGetPosition(), (int)ball.getRadius(), Obstacle))
				{
					ball.colisionWithP1 = !ball.getColisionWithP1();
					ball.setSpeed({ ball.getSpeed().x * INVERTMULTIPLIER, ball.getSpeed().y });
				}
			}

			actualPowerUp = ball.checkColisionWihtPowerUps(pUP);
			if (actualPowerUp != NONE)
			{
				pUP.setPosition({ OFFSCREENPOS, OFFSCREENPOS });
				powerUpEffect();
			}
		}

		puntoPara = 0;
		if (p1.getPoints() >= pointsToWin || p2.getPoints() >= pointsToWin)
		{
			switchInGame();
			gamestatus = CREDITS;
			if (p1.getPoints() >= pointsToWin) winer = p1;
			if (p2.getPoints() >= pointsToWin) winer = p2;
		}

		if (isPvp == false)
		{
			p2.iaMovement(ball.ballGetPosition());
		}
	}
	void Game::draw()
	{


		ball.drawMe();
		if (ball2Active)
		{
			ball2.drawMe();
		}
		if (ObstacleActive) DrawRectangle((int)Obstacle.x, (int)Obstacle.y, (int)Obstacle.width, (int)Obstacle.height, WHITE);
		p1.drawMe();
		p2.drawMe();
		pUP.drawMe();
		DrawText(TextFormat("TIME %1i", time[MINUTES]), screenWidth / HALFDIVIDER - MeasureText("TIME 100 :", 40) / HALFDIVIDER, screenHeight / 8 - 25, 25, GRAY);
		DrawText(TextFormat(": %1i", time[SECONDS]), screenWidth / HALFDIVIDER - MeasureText(".", 40) / 2, screenHeight / 8 - 25, 25, GRAY);
		if (pause == true)
		{
			DrawText("GAME PAUSED", screenWidth / 2 - MeasureText("GAME PAUSED", 40) / HALFDIVIDER, screenHeight / HALFDIVIDER - 40, 40, GRAY);
			DrawText("M to go back to menu, R to reset the game", screenWidth / HALFDIVIDER - MeasureText("M to go back to menu, R to reset the game", 20) / 2, screenHeight / 2, 20, GRAY);
		}

		

	}
	void Game::gameTime() {
		if (pause != true)
			seconds += GetFrameTime();
		if (seconds >= 1) {
			time[SECONDS]++;
			if (time[SECONDS] >= 60) {
				time[SECONDS] = 0;
				time[MINUTES]++;
			}
			if (time[MINUTES] >= 60) {
				time[MINUTES] = 0;
				time[HOURS]++;
			}
			seconds = 0;
		}
		if (time[SECONDS] == POWERUPAPPEARTIME) {
			pUP.init();
		}
	}

	void Game::switchInGame()
	{
		inGame = !inGame;
	}

	void Game::setGameInT()
	{
		inGame = true;
	}
	void Game::setPlayersColor()
	{
		p1.setColor(MenuToGame::get_pc1());
		p2.setColor(MenuToGame::get_pc2());
	}
	void Game::setOwnGameStatus()
	{
		if (getIsPVP() == true) gamestatus = GAME;
		if (getIsPVP() == false) gamestatus = GAMEIA;
	}
	void Game::setPointsToWin()
	{
		pointsToWin = MenuToGame::get_pointsToWin();;
	}
	void Game::setIsPVP(bool ispvp)
	{
		isPvp = ispvp;
	}

	int Game::getPointsToWin()
	{
		return pointsToWin;
	}
	bool Game::getIsPVP()
	{
		return isPvp;
	}


	void Game::powerUpEffect()
	{
		//ESCUDO, AGRANDARPADDLE, ACHICARPADDLERIV, MASVELOCIDAD, MULTIBALL, OBSTACULO, INVERTIRV, INVERTIRCONTROLES
		switch (actualPowerUp)
		{
		case ESCUDO:
			if (ball.getColisionWithP1())
			{
				p1.swapShield();
			}
			else
			{
				p2.swapShield();
			}
			break;
		case AGRANDARPADDLE:
			if (ball.getColisionWithP1())
			{
				p1.setHeigh(p1.getHeigh() + (p1.getHeigh() / HALFDIVIDER));
			}
			else
			{
				p2.setHeigh(p2.getHeigh() + (p2.getHeigh() / HALFDIVIDER));
			}
			break;
		case ACHICARPADDLERIV:
			if (ball.getColisionWithP1())
			{
				p2.setHeigh(p2.getHeigh() - (p2.getHeigh() / THIRDDIVIDER));
			}
			else
			{
				p1.setHeigh(p1.getHeigh() - (p1.getHeigh() / THIRDDIVIDER));
			}
			break;
		case MASVELOCIDAD:
			if (ball.getColisionWithP1())
			{
				p1.setPSpeed(p1.getPSpeed() + (p1.getPSpeed() / HALFDIVIDER));
			}
			else
			{
				p2.setPSpeed(p2.getPSpeed() + (p2.getPSpeed() / HALFDIVIDER));
			}

			break;
		case MULTIBALL:
			ball2Active = true;
			break;
		case OBSTACULO:
			ObstacleActive = true;
			break;
		case INVERTIRV:
			ball.setSpeed({ ball.getSpeed().x * INVERTMULTIPLIER, ball.getSpeed().y * INVERTMULTIPLIER });
			ball.colisionWithP1 = !ball.colisionWithP1;
			break;
		case INVERTIRCONTROLES:
			if (ball.getColisionWithP1())
			{
				p2.swapInvControls();
			}
			else
			{
				p1.swapInvControls();
			}
			break;
		default:
			break;
		}
	}

}