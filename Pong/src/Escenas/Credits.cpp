#include "Credits.h"


namespace PONGJB
{
	Credits::Credits()
	{
		gamestatus = CREDITS;
		mycolor = BLACK;
	}
	Credits::~Credits()
	{
	}
	void Credits::init()
	{
		gamestatus = CREDITS;
	}
	void Credits::input()
	{
		if (IsKeyReleased(KEY_ENTER))
		{
			gamestatus = MENU;
		}
		if (IsKeyReleased(KEY_ESCAPE))
		{
			gamestatus = MENU;
		}
	}
	void Credits::update(Player winer)
	{
		mycolor = winer.getColor();
	}
	void Credits::draw()
	{
		DrawText("Victory", GetScreenWidth()/ HALFDIVIDER - MeasureText("Victory", TextSize)/ HALFDIVIDER, GetScreenHeight()/ HALFDIVIDER - TextSize*4, TextSize, mycolor);

		DrawText("Created by Julian Bega", GetScreenWidth() / HALFDIVIDER - MeasureText("Created by Julian Bega", TextSize) / 2, GetScreenHeight() / HALFDIVIDER , TextSize, LIGHTGRAY);
		DrawText("Materia Practica profesional 1, TSDV", GetScreenWidth() / HALFDIVIDER - MeasureText("Materia Practica profesional 1, TSDV", TextSize) / HALFDIVIDER, (GetScreenHeight() / HALFDIVIDER) + TextSize, TextSize, LIGHTGRAY);
		DrawText("Using Raylib", GetScreenWidth() / HALFDIVIDER - MeasureText("Using Raylib", TextSize) / HALFDIVIDER, (GetScreenHeight() / HALFDIVIDER) + (TextSize * DOUBLE), TextSize, LIGHTGRAY);
		DrawText("Press Enter/Escape to go back to menu", GetScreenWidth() / HALFDIVIDER - MeasureText("Press Enter/Escape to go back to menu", TextSize) / HALFDIVIDER, (GetScreenHeight() / HALFDIVIDER) + (TextSize * TRIPLE), TextSize, LIGHTGRAY);
		DrawText("Sound effects obtained from https ://www.zapsplat.com", GetScreenWidth() / HALFDIVIDER - MeasureText("Sound effects obtained from https ://www.zapsplat.com", TextSize/HALFDIVIDER) / HALFDIVIDER, (GetScreenHeight() / HALFDIVIDER) + (TextSize * 4), TextSize / HALFDIVIDER, LIGHTGRAY);
		DrawText("Pong version 1 . 2", GetScreenWidth() / HALFDIVIDER - MeasureText("Pong version 1 . 2", TextSize / HALFDIVIDER) / HALFDIVIDER, (GetScreenHeight() / HALFDIVIDER) + (TextSize * 5), TextSize / HALFDIVIDER, LIGHTGRAY);
	}
	void Credits::setOwnGameStatus()
	{
		gamestatus = CREDITS;
	}
}