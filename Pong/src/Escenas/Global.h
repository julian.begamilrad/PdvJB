#ifndef GLOBAL_H
#define GLOBAL_H
namespace PONGJB
{
	const int TextSize = 30;
	const int HEIGHT = 80;
	const int WIDTH = 20;
	const int QUARTERDIVIDER = 4;
	const int HALFDIVIDER = 2;
	const int THIRDDIVIDER = 3;
	const int INVERTMULTIPLIER = -1;
	const int DOUBLE = 2;
	const int TRIPLE = 3;
	const float PLAYERSPEED = 0.3f;
	const int POWERUPAPPEARTIME = 10;
	const int OFFSCREENPOS = -10;
	const int BASEPOINTSTOWIN = 2;
	const int PLAYERCOLORSCANT = 6;
	enum GameStage
	{
		MENU, GAME, CREDITS, EXIT, GAMEIA
	};

	enum POWERUPTYPES
	{
		ESCUDO, AGRANDARPADDLE, ACHICARPADDLERIV, MASVELOCIDAD, MULTIBALL, OBSTACULO, INVERTIRV, INVERTIRCONTROLES, NONE=50
	};


}

#endif