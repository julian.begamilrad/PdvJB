#ifndef GAMESTRUCTURE_H
#define GAMESTRUCTURE_H

#include "Global.h"
#include "Game.h"
#include "Menu.h"
#include "Credits.h"

namespace PONGJB
{
	
	class GameStructure
	{
	public:
		
		GameStructure();
		~GameStructure();
		void gameLoop();
		void gameInit();
		void input();
		void update();
		void setConfig();
		void draw();
		void setConfig(bool pvp);
		void deInit();
		const int screenWidth = 810;
		const int screenHeight = 450;

		GameStage gamestatus;
		GameStage lastGamestatus;

		Game* game;
		Menu* menu;
		Credits* credits;
		bool firstTime;
		int pointsToWin;

		

	private:
		bool inGame;

	};
}
#endif