#ifndef CREDITS_H
#define CREDITS_H
#include "raylib.h"
#include "Global.h"
#include "../Objetos/Player.h"

namespace PONGJB
{
	class Credits
	{
	public:
		Credits();
		~Credits();
		void init();
		void input();
		void update(Player winer);
		void draw();
		GameStage gamestatus;
		Color mycolor;
		void setOwnGameStatus();
	private:
	};
}
#endif