#include "GameStructure.h"
namespace PONGJB
{
	GameStructure::GameStructure()
	{
		InitWindow(screenWidth, screenHeight, "Pong v0.1");
		InitAudioDevice();
		game = new Game();
		menu = new Menu();
		credits = new Credits();
		gamestatus = MENU;
		lastGamestatus = MENU;
		firstTime = true;
		pointsToWin = BASEPOINTSTOWIN;
		inGame = false;
		
		
	}
	GameStructure::~GameStructure()
	{
		deInit();
	}
	void GameStructure::gameLoop()
	{
		gameInit();
		while (inGame && !WindowShouldClose())
		{
			BeginDrawing();
			ClearBackground(BLACK);
			input();
			update();
			draw();			
		}
	}
	void GameStructure::gameInit()
	{
		pointsToWin = BASEPOINTSTOWIN;
		menu->init();
		game->init();
		credits->init();		
		gamestatus = MENU;
		lastGamestatus = gamestatus;
		inGame = true;
	}
	void GameStructure::input()
	{
		switch (gamestatus)
		{
		case MENU:
			menu->input();
			break;
		case GAME:
			game->input();
			break;
		case GAMEIA:
			game->input();
			break;
		case CREDITS:
			credits->input();
			
			break;
		case EXIT:
			inGame = false;
			break;
		}
	}
	void GameStructure::update()
	{
		if (lastGamestatus != gamestatus)setConfig();
		switch (gamestatus)
		{
		case MENU:
			menu->update();
			lastGamestatus = gamestatus;
			gamestatus = menu->gamestatus;
			game->setGameInT();
			break;
		case GAME:
			lastGamestatus = gamestatus;
			game->update(); 
			gamestatus = game->gamestatus;
			break;
		case GAMEIA:
			lastGamestatus = gamestatus;
			game->update();
			gamestatus = game->gamestatus;
			break;
		case CREDITS:
			lastGamestatus = gamestatus;
			credits->update(game->winer);
			gamestatus = credits->gamestatus;
			break;
		case EXIT:
			inGame = false;
			break;
		}
	}
	void GameStructure::setConfig()
	{
		switch (gamestatus)
		{
		case MENU:
			credits->setOwnGameStatus();
			pointsToWin = menu->pointsRequisite;
			game->setOwnGameStatus();
			break;
		case GAME:
			setConfig(true);
			menu->setOwnGameStatus();
			break;
		case GAMEIA:
			setConfig(false);
			menu->setOwnGameStatus();
			break;
		case CREDITS:
			menu->setOwnGameStatus();
			game->setOwnGameStatus();
			game->setGameInT();
			game->init();
			break;
		case EXIT:
			inGame = false;
			break;
		}
	}
	void GameStructure::draw()
	{
		switch (gamestatus)
		{
		case MENU:
			menu->draw();			
			break;
		case GAME:
			game->draw();
			break;
		case GAMEIA:
			game->draw();
			break;
		case CREDITS:
			credits->draw();
			break;
		case EXIT:
			inGame = false;
			break;
		}

		EndDrawing();
	}
	void GameStructure::setConfig(bool pvp)
	{
		game->setPlayersColor();
		game->setIsPVP(pvp);
		game->setPointsToWin();
		if (game->gamestatus != MENU) game->setOwnGameStatus();		
	}
	void GameStructure::deInit()
	{
		
		CloseAudioDevice();     // Close audio device
		CloseWindow();
	}
}