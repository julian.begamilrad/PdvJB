#include "Menu.h"

namespace PONGJB
{
	Menu::Menu()
	{
		p1Color = BLUE;
		Ip1Color = 0;
		p2Color = RED;
		Ip2Color = 1;
		pointsRequisite = BASEPOINTSTOWIN;
		actualOption = Play;
		gamestatus = MENU;
		actualGameStatus = NONE;
		isControlMenu = false;
		pickingGame = false;
		select = LoadSound("res/Select.ogg");
		moveSelection = LoadSound("res/MoveSelection.ogg");
	}
	Menu::~Menu()
	{
		UnloadSound(select);     // Unload sound data
		UnloadSound(moveSelection);
	}
	void Menu::init()
	{
		
		pointsRequisite = BASEPOINTSTOWIN;
		actualOption = Play;
		gamestatus = MENU;
		actualGameStatus = NONE;
		isControlMenu = false;
		pickingGame = false;
	}
	void Menu::input()
	{
		if (IsKeyReleased(KEY_W) || IsKeyReleased(KEY_UP))
		{
			PlaySound(moveSelection);
			if (actualGameStatus == NONE)
			{
				if (actualOption == Play)
				{
					actualOption = Exit;
				}
				else
				{
					actualOption--;
				}
			}
			else {

				if (actualGameStatus == PVP)
				{
					actualGameStatus = BACK;
				}
				else if (actualGameStatus == VSIA)
				{
					actualGameStatus = PVP;
				}
				else if (actualGameStatus == PUNTOS)
				{
					actualGameStatus = VSIA;
				}
				else 
				{
					actualGameStatus = PUNTOS;
				}
			}
		}
		if (IsKeyReleased(KEY_S) || IsKeyReleased(KEY_DOWN))
		{
			PlaySound(moveSelection);
			if (actualGameStatus == NONE)
			{
				if (actualOption == Exit)
				{
					actualOption = Play;
				}
				else
				{
					actualOption++;
				}
			}
			else
			{
				if (actualGameStatus == PVP)
				{
					actualGameStatus = VSIA;
				}
				else if (actualGameStatus == VSIA)
				{
					actualGameStatus = PUNTOS;
				}
				else if (actualGameStatus == PUNTOS)
				{
					actualGameStatus = BACK;
				}
				else
				{
					actualGameStatus = PVP;
				}
			}
		}
		if (IsKeyReleased(KEY_A))
		{
			if (actualGameStatus == PUNTOS) {
				pointsRequisite --;
			}
			if (isControlMenu == true)
			{
				if (Ip1Color < PLAYERCOLORSCANT-1) //RESTO UNO PORQUE EST� DENTRO DE UN VECTOR POR LO QUE SON 6 POS CONTANDO LA 0
				{
					Ip1Color++;
				}
				else
				{
					Ip1Color = 0;
				}
			}
		}
		if (IsKeyReleased(KEY_D))
		{
			if (actualGameStatus == PUNTOS) {
				pointsRequisite++;
			}
			if (isControlMenu == true)
			{
				if (Ip1Color > 0)
				{
					Ip1Color--;
				}
				else
				{
					Ip1Color = PLAYERCOLORSCANT - 1; //RESTO UNO PORQUE EST� DENTRO DE UN VECTOR POR LO QUE SON 6 POS CONTANDO LA 0
				}
			}
		}
		if (IsKeyReleased(KEY_RIGHT)) 
		{
			if (isControlMenu == true)
			{
				if (Ip2Color > 0)
				{
					Ip2Color--;
				}
				else
				{
					Ip2Color = PLAYERCOLORSCANT - 1; //RESTO UNO PORQUE EST� DENTRO DE UN VECTOR POR LO QUE SON 6 POS CONTANDO LA 0
				}
			}
		}
		if (IsKeyReleased(KEY_LEFT)) 
		{
			if (isControlMenu == true)
			{
				if (Ip2Color < PLAYERCOLORSCANT - 1) //RESTO UNO PORQUE EST� DENTRO DE UN VECTOR POR LO QUE SON 6 POS CONTANDO LA 0
				{
					Ip2Color++;
				}
				else
				{
					Ip2Color = 0;
				}
			}
		}
		if (IsKeyReleased(KEY_ENTER))
		{
			PlaySound(select);
			if (isControlMenu == false)
			{
				if (actualGameStatus != NONE)
				{
					if (actualGameStatus == BACK) {
						this->init();
					}
					if (actualGameStatus == PVP) {
						gamestatus = GAME;
					}
					else if (actualGameStatus == VSIA)
					{
						gamestatus = GAMEIA;
					}

				}
				else {

					switch (actualOption)
					{
					case Play:
						if (pickingGame == false) {
							actualGameStatus = PVP;
							pickingGame = true;
						}


						break;
					case Controls:
						isControlMenu = true;
						break;

					case Credits:
						gamestatus = CREDITS;
						break;

					case Exit:
						gamestatus = EXIT;
						break;
					default:
						break;
					}
				}
			}
			else
			{
				isControlMenu = false;
			}
		}
		if (IsKeyDown(KEY_ESCAPE))
		{
			
			if (actualGameStatus == NONE) 
			{
				isControlMenu = false;
			}
			else 
			{
				actualGameStatus = NONE;
			}
		}


	}
	void Menu::update()
	{
		MenuToGame::set_pc1(colores[Ip1Color]);
		MenuToGame::set_pc2(colores[Ip2Color]);
		MenuToGame::set_pointsToWin(pointsRequisite);
		p1Color = colores[Ip1Color];
		p2Color = colores[Ip2Color];
	}
	void Menu::draw()
	{

		if (isControlMenu == false)
		{
			DrawText("Pong version 1 . 2", GetScreenWidth() / HALFDIVIDER - MeasureText("Pong version 1 . 2", TextSize / HALFDIVIDER) / HALFDIVIDER, (GetScreenHeight() / HALFDIVIDER) + (TextSize * 7), TextSize / HALFDIVIDER, LIGHTGRAY);
			DrawText("Play", DISTANCEDEFAULT * TEXTDISTANCEINX, DISTANCEDEFAULT * PLAYTEXTDISTANCEINY, DISTANCEDEFAULT, WHITE);
			DrawText("Controls", DISTANCEDEFAULT * TEXTDISTANCEINX, DISTANCEDEFAULT * CONTROLESTEXTDISTANCEINY, DISTANCEDEFAULT, WHITE);
			DrawText("Credits", DISTANCEDEFAULT * TEXTDISTANCEINX, DISTANCEDEFAULT * CREDITSTEXTDISTANCEINY, DISTANCEDEFAULT, WHITE);
			DrawText("Exit", DISTANCEDEFAULT * TEXTDISTANCEINX, DISTANCEDEFAULT * EXITTEXTDISTANCEINY, DISTANCEDEFAULT, WHITE);

			switch (actualOption)
			{
			case Play:
				DrawText("Play", DISTANCEDEFAULT * TEXTDISTANCEINX, DISTANCEDEFAULT * PLAYTEXTDISTANCEINY, DISTANCEDEFAULT, YELLOW);
				break;

			case Controls:
				DrawText("Controls", DISTANCEDEFAULT * TEXTDISTANCEINX, DISTANCEDEFAULT * CONTROLESTEXTDISTANCEINY, DISTANCEDEFAULT, YELLOW);
				break;

			case Credits:
				DrawText("Credits", DISTANCEDEFAULT * TEXTDISTANCEINX, DISTANCEDEFAULT * CREDITSTEXTDISTANCEINY, DISTANCEDEFAULT, YELLOW);
				break;

			case Exit:
				DrawText("Exit", DISTANCEDEFAULT * TEXTDISTANCEINX, DISTANCEDEFAULT * EXITTEXTDISTANCEINY, DISTANCEDEFAULT, YELLOW);
				break;
			default:
				break;
			}

			//DrawText("F2 - volume", GetScreenWidth() - (DISTANCEDEFAULT * CONTROLESTEXTDISTANCEINY), DISTANCEDEFAULT * EXITTEXTDISTANCEINY, DISTANCEDEFAULT, WHITE);
			//DrawText("F3 + volume", GetScreenWidth() - (DISTANCEDEFAULT * CONTROLESTEXTDISTANCEINY), (DISTANCEDEFAULT * EXITTEXTDISTANCEINY) + DISTANCEDEFAULT, DISTANCEDEFAULT, WHITE);

			if (actualGameStatus != NONE)
			{
				if (actualGameStatus == PVP)
				{
					DrawText("PVP", DISTANCEDEFAULT * TEXTDISTANCEINX + (DISTANCEDEFAULT * 6), DISTANCEDEFAULT * PLAYTEXTDISTANCEINY, DISTANCEDEFAULT, YELLOW);
					DrawText("Player vs IA", DISTANCEDEFAULT * TEXTDISTANCEINX + (DISTANCEDEFAULT * 6), DISTANCEDEFAULT * CONTROLESTEXTDISTANCEINY, DISTANCEDEFAULT, WHITE);
					DrawText(TextFormat("Punt para ganar %1i", pointsRequisite), DISTANCEDEFAULT * TEXTDISTANCEINX + (DISTANCEDEFAULT * 6), DISTANCEDEFAULT * CREDITSTEXTDISTANCEINY, DISTANCEDEFAULT, WHITE);
					DrawText("Back", DISTANCEDEFAULT * TEXTDISTANCEINX + (DISTANCEDEFAULT * 6), DISTANCEDEFAULT * EXITTEXTDISTANCEINY, DISTANCEDEFAULT, WHITE);

				}
				else if (actualGameStatus == VSIA)
				{
					DrawText("PVP", DISTANCEDEFAULT * TEXTDISTANCEINX + (DISTANCEDEFAULT * 6), DISTANCEDEFAULT * PLAYTEXTDISTANCEINY, DISTANCEDEFAULT, WHITE);
					DrawText("Player vs IA", DISTANCEDEFAULT * TEXTDISTANCEINX + (DISTANCEDEFAULT * 6), DISTANCEDEFAULT * CONTROLESTEXTDISTANCEINY, DISTANCEDEFAULT, YELLOW);
					DrawText(TextFormat("Punt para ganar %1i", pointsRequisite), DISTANCEDEFAULT * TEXTDISTANCEINX + (DISTANCEDEFAULT * 6), DISTANCEDEFAULT * CREDITSTEXTDISTANCEINY, DISTANCEDEFAULT, WHITE);
					DrawText("Back", DISTANCEDEFAULT * TEXTDISTANCEINX + (DISTANCEDEFAULT * 6), DISTANCEDEFAULT * EXITTEXTDISTANCEINY, DISTANCEDEFAULT, WHITE);
				}
				else if (actualGameStatus == PUNTOS)
				{
					DrawText("PVP", DISTANCEDEFAULT * TEXTDISTANCEINX + (DISTANCEDEFAULT * 6), DISTANCEDEFAULT * PLAYTEXTDISTANCEINY, DISTANCEDEFAULT, WHITE);
					DrawText("Player vs IA", DISTANCEDEFAULT * TEXTDISTANCEINX + (DISTANCEDEFAULT * 6), DISTANCEDEFAULT * CONTROLESTEXTDISTANCEINY, DISTANCEDEFAULT, WHITE);
					DrawText(TextFormat("Punt para ganar %1i", pointsRequisite), DISTANCEDEFAULT * TEXTDISTANCEINX + (DISTANCEDEFAULT * 6), DISTANCEDEFAULT * CREDITSTEXTDISTANCEINY, DISTANCEDEFAULT, YELLOW);
					DrawText("Back", DISTANCEDEFAULT * TEXTDISTANCEINX + (DISTANCEDEFAULT * 6), DISTANCEDEFAULT * EXITTEXTDISTANCEINY, DISTANCEDEFAULT, WHITE);
				}
				else
				{
					DrawText("PVP", DISTANCEDEFAULT * TEXTDISTANCEINX + (DISTANCEDEFAULT * 6), DISTANCEDEFAULT * PLAYTEXTDISTANCEINY, DISTANCEDEFAULT, WHITE);
					DrawText("Player vs IA", DISTANCEDEFAULT * TEXTDISTANCEINX + (DISTANCEDEFAULT * 6), DISTANCEDEFAULT * CONTROLESTEXTDISTANCEINY, DISTANCEDEFAULT, WHITE);
					DrawText(TextFormat("Punt para ganar %1i", pointsRequisite), DISTANCEDEFAULT * TEXTDISTANCEINX + (DISTANCEDEFAULT * 6), DISTANCEDEFAULT * CREDITSTEXTDISTANCEINY, DISTANCEDEFAULT, WHITE);
					DrawText("Back", DISTANCEDEFAULT * TEXTDISTANCEINX + (DISTANCEDEFAULT * 6), DISTANCEDEFAULT * EXITTEXTDISTANCEINY, DISTANCEDEFAULT, YELLOW);
				}
			}
		}

		if (isControlMenu == true)
		{
			DrawRectangle(GetScreenWidth()/QUARTERDIVIDER, (GetScreenHeight() / HALFDIVIDER) - (HEIGHT / QUARTERDIVIDER)*3, WIDTH, HEIGHT, p1Color);
			DrawRectangle(GetScreenWidth() / QUARTERDIVIDER, (GetScreenHeight() / HALFDIVIDER) + (HEIGHT / QUARTERDIVIDER) + WIDTH, WIDTH, WIDTH, WHITE	);
			DrawRectangle(GetScreenWidth() / QUARTERDIVIDER, (GetScreenHeight() / HALFDIVIDER) + (HEIGHT / QUARTERDIVIDER) + WIDTH* TRIPLE, WIDTH, WIDTH, WHITE);
			DrawText("W", GetScreenWidth() / QUARTERDIVIDER + 3, (GetScreenHeight() / HALFDIVIDER) + (HEIGHT / QUARTERDIVIDER) + WIDTH, WIDTH, BLACK);
			DrawText("S", GetScreenWidth() / QUARTERDIVIDER +3, (GetScreenHeight() / HALFDIVIDER) + (HEIGHT / QUARTERDIVIDER) + WIDTH * TRIPLE, WIDTH, BLACK);



			DrawText("press N to mute music in-game", GetScreenWidth() / HALFDIVIDER - MeasureText(".", 40) / HALFDIVIDER, GetScreenHeight() - 25, 25, GRAY);
			// tengo que poner que con N se mutea la musica y con que teclas se mueve el player 2
			DrawRectangle((GetScreenWidth()/ QUARTERDIVIDER)* TRIPLE, GetScreenHeight()/ HALFDIVIDER - (HEIGHT/ QUARTERDIVIDER)* TRIPLE, WIDTH, HEIGHT, p2Color);
			DrawText("Up arrow", GetScreenWidth() / QUARTERDIVIDER * TRIPLE, (GetScreenHeight() / HALFDIVIDER) + (HEIGHT / QUARTERDIVIDER) + WIDTH, WIDTH, WHITE);
			DrawText("Down arrow", GetScreenWidth() / QUARTERDIVIDER * TRIPLE, (GetScreenHeight() / HALFDIVIDER) + (HEIGHT / QUARTERDIVIDER) + WIDTH * TRIPLE, WIDTH, WHITE);
		}
	}

	

	void Menu::setOwnGameStatus()
	{
		gamestatus = MENU;
	}

	bool Menu::getIsControlMenu()
	{
		return isControlMenu;
	}

	void Menu::changeIsControlMenu()
	{
		isControlMenu = !isControlMenu;
	}
}