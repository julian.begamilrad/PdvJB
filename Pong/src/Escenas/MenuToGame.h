#ifndef MENUTOGAME_H
#define MENUTOGAME_H
#include "raylib.h"
#include "Global.h"
namespace PONGJB
{
	static class MenuToGame
	{
	public:
		static void set_pc1(Color p1color);
		static void set_pc2(Color p2color);
		static void set_pointsToWin(int ptw);

		static int get_pointsToWin();
		static Color get_pc1();
		static Color get_pc2();
		
		static Color _pc1;
		static Color _pc2;
		static int _pointsToWin;
	private:
		
	};
}
#endif MENUTOGAME_H