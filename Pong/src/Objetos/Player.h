#ifndef PLAYER_H
#define PLAYER_H
#include "raylib.h"
#include "../Escenas/Global.h"
namespace PONGJB
{	
	 
	class Player
	{
	public:
		Player();
		~Player();
		void init(int Nplayer);
		void drawMe();
		void input();
		void addPoint();
		void iaMovement(Vector2 ballPos);
				
		int getPoints();
		Rectangle getBody();
		Color getColor();
		float getPSpeed();
		float getHeigh();
		bool getShield();
		bool getInvControls();

		void swapInvControls();
		void swapShield();

		void setPosition(Vector2 newPos);
		void setColor(Color nColor);
		void setHeigh(float nHeigh);
		void setPSpeed(float nSpeed);

		int player;
	private:
		bool invControls;
		bool shield;
		float pSpeed;
		Color myColor;
		Rectangle myBody;
		int points;	
		void moveUp();
		void moveDown();
		Vector2 playerGetPosition();
	};
}
#endif