#include "PowerUp.h"


namespace PONGJB
{
	PowerUp::PowerUp()
	{
		position = {0,0};
		radius = POWERUPRADIUS;
		myColor = BLACK;
		colisionWithP1 = false;
		isTimingPowerUp = NULL;
		active = false;
		type = NONE;

	}
	PowerUp::~PowerUp()
	{
	}
	void PowerUp::init()
	{
		srand(time(NULL));
		float Xpos =( (GetScreenWidth() / QUARTERDIVIDER) + rand() %(GetScreenWidth() / HALFDIVIDER));
		float Ypos = (POWERUPRADIUS)+ rand() % (GetScreenHeight() - POWERUPRADIUS);
		position = { Xpos,Ypos };
		radius = POWERUPRADIUS;
		myColor = PINK;
		colisionWithP1 = false;
		isTimingPowerUp = NULL;
		active = true;
		//type = OBSTACULO;
		type = rand() % (INVERTIRCONTROLES);
	}
	void PowerUp::drawMe()
	{
		if (active)
		{
			DrawCircle((int)position.x, (int)position.y, radius, myColor);
		}
	}
	
	Vector2 PowerUp::powerUpGetPosition()
	{
		return position;
	}
	int PowerUp::getType()
	{
		return type;
	}

	void PowerUp::setPosition(Vector2 nPosition)
	{
		position = nPosition;
	}


	
}