#include "Player.h"

namespace PONGJB
{
	Player::Player()
	{
		myBody.height = 0;
		myBody.width = 0;
		myBody.y = 0;
		myBody.x = 0;
		myColor = WHITE;
		pSpeed = 0;
		points = 0;
		player = 0;
		shield = false;
		invControls = false;
	}

	Player::~Player() {

	}
	void Player::init(int Nplayer)
	{
		invControls = false;
		myBody.height = HEIGHT;
		myBody.width = WIDTH;
		myBody.y = GetScreenHeight() - (GetScreenHeight() / HALFDIVIDER) - (HEIGHT / HALFDIVIDER);
		player = Nplayer;
		shield = false;
		switch (player)
		{
		case (1):
		{
			myBody.x = WIDTH;
		}
		break;
		case (2):
		{
			myBody.x = GetScreenWidth() - (WIDTH * HALFDIVIDER);
		}
		break;
		default:
			break;
		}

		pSpeed = PLAYERSPEED;
		points = 0;
	}
	void Player::drawMe() {

		DrawRectangle(myBody.x, myBody.y, myBody.width, myBody.height, myColor);
		switch (player)
		{
		case (1):
		{
			DrawText(TextFormat("P1 %1i", points), 100, 40, 30, LIGHTGRAY);
			if (shield)
			{
				DrawRectangle(0, 0, 4, GetScreenHeight(), SKYBLUE);
			}
		}
		break;
		case (2):
		{
			DrawText(TextFormat("P2 %1i", points), (GetScreenWidth() - 170), 40, 30, LIGHTGRAY);
			if (shield)
			{
			DrawRectangle(GetScreenWidth()-4, 0, 4, GetScreenHeight(), SKYBLUE);
			}
		}
		break;
		default:
			break;
		}

	}
	void Player::input()
	{
		if (invControls == false)
		{
			switch (player)
			{
			case (1):
			{
				if (IsKeyDown(KEY_W) && this->playerGetPosition().y >= 0)
				{
					moveUp();
				}
				if (IsKeyDown(KEY_S) && this->playerGetPosition().y <= (GetScreenHeight() - this->myBody.height))
				{
					moveDown();
				}
			}
			break;
			case (2):
			{
				if (IsKeyDown(KEY_UP) && this->playerGetPosition().y >= 0)
				{
					moveUp();
				}
				if (IsKeyDown(KEY_DOWN) && this->playerGetPosition().y <= (GetScreenHeight() - this->myBody.height))
				{
					moveDown();
				}
			}
			break;
			default:
				break;
			}
		}
		else 
		{
			switch (player)
			{
			case (1):
			{
				if (IsKeyDown(KEY_S) && this->playerGetPosition().y >= 0 && invControls)
				{
					moveUp();
				}
				if (IsKeyDown(KEY_W ) && this->playerGetPosition().y <= (GetScreenHeight() - this->myBody.height))
				{
					moveDown();
				}
			}
			break;
			case (2):
			{
				if (IsKeyDown(KEY_DOWN) && this->playerGetPosition().y >= 0)
				{
					moveUp();
				}
				if (IsKeyDown(KEY_UP ) && this->playerGetPosition().y <= (GetScreenHeight() - this->myBody.height))
				{
					moveDown();					
				}
			}
			break;
			default:
				break;
			}
		}
	}
	
	void Player::addPoint()
	{
		points = points + 1;
	}
	void Player::iaMovement(Vector2 ballPos)
	{
		if ((myBody.y - (myBody.height / HALFDIVIDER)) < ballPos.y && this->playerGetPosition().y <= (GetScreenHeight() - this->myBody.height))
		{
			moveUp();
		}
		if ((myBody.y + (myBody.height / HALFDIVIDER)) > ballPos.y && this->playerGetPosition().y >= 0)
		{
			moveDown();
		}
	}

	


	int Player::getPoints()
	{
		return points;
	}
	Rectangle Player::getBody()
	{
		return myBody;
	}
	Color Player::getColor()
	{
		return myColor;
	}
	float Player::getPSpeed()
	{
		return pSpeed;
	}
	float Player::getHeigh()
	{
		return myBody.height;
	}
	bool Player::getShield()
	{
		return shield;
	}
	bool Player::getInvControls()
	{
		return invControls;
	}

	void Player::swapInvControls()
	{
		invControls = !invControls;
	}
	void Player::swapShield()
	{
		shield = !shield;
	}
	void Player::setPosition(Vector2 newPos) {
		myBody.x = newPos.x;
		myBody.y = newPos.y;
	}
	void Player::setColor(Color nColor)
	{
		myColor = nColor;
	}
	void Player::setHeigh(float nHeigh)
	{
		myBody.height = nHeigh;
	}
	void Player::setPSpeed(float nSpeed)
	{
		pSpeed = nSpeed;
	}

	void Player::moveUp()
	{
		Vector2 NewPosition = this->playerGetPosition();
		NewPosition.y = NewPosition.y - pSpeed;
		this->setPosition(NewPosition);
	}
	void Player::moveDown()
	{
		Vector2 NewPosition = this->playerGetPosition();
		NewPosition.y = NewPosition.y + pSpeed;
		this->setPosition(NewPosition);
	}
	Vector2 Player::playerGetPosition() {
		Vector2 Position = { myBody.x, myBody.y };
		return Position;
	}
}