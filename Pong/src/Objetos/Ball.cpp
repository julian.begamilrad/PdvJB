#include "Ball.h"

namespace PONGJB
{
	PONGJB::Ball::Ball()
	{
		init();
	}
	Ball::~Ball()
	{
	}
	
	void Ball::init()
	{
		srand(time(NULL));
		int inicio = (-4) + rand() % (7);
		float centroX = GetScreenWidth() / 2;
		float centroY = GetScreenHeight() / 2;
		position = { centroX , centroY };
		radius= RADIUS;
		myColor = WHITE;
		switch (inicio)
		{
			case -4:
				speed = { 0.08f, 0.1f };
				colisionWithP1 = true;
				break;
			case -3:
				speed = { 0.08f, 0.05f };
				colisionWithP1 = true;
				break;
			case -2:
				speed = { 0.08f, -0.05f };
				colisionWithP1 = true;
				break;
			case -1:
				speed = { 0.08f, -0.1f };
				colisionWithP1 = true;
				break;
			case 0:
				speed = { -0.08f, -0.1f };
				colisionWithP1 = false;
				break;
			case 1:
				speed = { -0.08f, -0.05f };
				colisionWithP1 = false;
				break;
			case 2:
				speed = { -0.08f, 0.1f };
				colisionWithP1 = false;
				break;
			case 3:
				speed = { -0.08f, 0.05f };
				colisionWithP1 = false;
				speed = { 0.05f, 0.1f };
				colisionWithP1 = true;
				break;
			
		default:
			break;
		}
		
	}
	void Ball::drawMe()
	{
		DrawCircle((int)position.x, (int)position.y, radius, myColor);
	}
	void Ball::move()
	{
		this->position.x += this->speed.x;
		this->position.y += this->speed.y;
	}
	int Ball::testLimits()
	{
		if (this->ballGetPosition().y <= radius) { this->speed.y = (this->speed.y * (-1)); return 0; }
		if (this->ballGetPosition().y >= (GetScreenHeight() - radius)) {this->speed.y = (this->speed.y * (-1)); return 0;	}
		if (this->ballGetPosition().x <= radius) {  this->init(); return 2; }
		if (this->ballGetPosition().x >= (GetScreenWidth() - radius)) { this->init(); return 1;}
	}

	void Ball::checkColisionWihtPlayers(Rectangle Player1, Rectangle Player2)
	{
		float myX = this->ballGetPosition().x;
		float myY = this->ballGetPosition().y;
		float myR = this->radius;


		float P1HalfHeight = Player1.y + (Player1.height / HALFDIVIDER);
		float P1TopQuarter = Player1.y + (Player1.height / QUARTERDIVIDER);
		float P1BottomQuarter = Player1.y + ((Player1.height / QUARTERDIVIDER) * TRIPLE);

		float P2HalfHeight = Player2.y + (Player2.height / HALFDIVIDER);
		float P2TopQuarter = Player2.y + (Player2.height / QUARTERDIVIDER);
		float P2BottomQuarter = Player2.y + ((Player2.height / QUARTERDIVIDER) * TRIPLE);



		if ((myX - myR) <= (Player1.x + Player1.width) && myY >= Player1.y && myY <= (Player1.y + Player1.height) && colisionWithP1 == false)
		{
			this->speed.x = (this->speed.x * (-(SPEEDMULTIPLIER)));
			colisionWithP1 = true;

			if (myY < P1TopQuarter) { this->speed.y = -(LONGANGLESPEED); }

			if (myY < P1HalfHeight && myY > P1TopQuarter) { this->speed.y = -(SHORTANGLESPEED); }

			if (myY > P1HalfHeight && myY < P1BottomQuarter) { this->speed.y = SHORTANGLESPEED; }

			if (myY > P1BottomQuarter) { this->speed.y = LONGANGLESPEED; }


		}
		if ((myX + myR) >= Player2.x && myY >= Player2.y && myY <= (Player2.y + Player2.height) && colisionWithP1 == true)
		{
			this->speed.x = (this->speed.x * (-(SPEEDMULTIPLIER)));
			colisionWithP1 = false;

			if (myY < P2TopQuarter) { this->speed.y = -(LONGANGLESPEED); }

			if (myY < P2HalfHeight && myY > P2TopQuarter) { this->speed.y = -(SHORTANGLESPEED); }

			if (myY > P2HalfHeight && myY < P2BottomQuarter) { this->speed.y = SHORTANGLESPEED; }

			if (myY > P2BottomQuarter) { this->speed.y = LONGANGLESPEED; }

			if ((myX - myR) <= Player1.x + Player1.width && (myY - myR) >= Player1.y && (myY + myR) <= (Player1.y + Player1.height) && colisionWithP1 == false)
			{
				this->speed.x = (this->speed.x * (-(SPEEDMULTIPLIER)));
				colisionWithP1 = true;
			}
			if ((myX + myR) >= Player2.x && (myY - myR) >= Player2.y && (myY + myR) <= (Player2.y + Player2.height) && colisionWithP1 == true)
			{
				this->speed.x = (this->speed.x * (-(SPEEDMULTIPLIER)));
				colisionWithP1 = false;
			}


	}

	}
	int Ball::checkColisionWihtPowerUps(PowerUp PowUP)
	{
		if(CheckCollisionCircles(position, RADIUS, PowUP.powerUpGetPosition(), POWERUPRADIUS))
		{
			return PowUP.getType();
		}
		else
		{
			return NONE;
		}
		
		
	}

	Vector2 Ball::getSpeed()
	{
		return speed;
	}
	int Ball::getRadius()
	{
		return radius;
	}
	Vector2 Ball::ballGetPosition()
	{
		return position;
	}
	bool Ball::getColisionWithP1()
	{
		return colisionWithP1;
	}
	

	void Ball::setSpeed(Vector2 nSpeed)
	{
		speed = nSpeed;
	}
	void Ball::setPosition(Vector2 newPos)
	{
		position = newPos;
	}
}
