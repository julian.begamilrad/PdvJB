#ifndef POWERUP_H
#define POWERUP_H

#include <iostream>
#include <stdlib.h>
#include <time.h>
#include "raylib.h"
#include "../Escenas/Global.h"


namespace PONGJB
{
	
	const int POWERUPRADIUS = 10;
	class PowerUp
	{
	public:
		PowerUp();
		~PowerUp();
		void init();
		void drawMe();
		
		Vector2 powerUpGetPosition();
		int getType();

		void setPosition(Vector2 nPosition);
		
		bool isTimingPowerUp;
		bool active;
		

	private:
		Vector2 position;
		int type;
		int radius;
		Color myColor;
		bool colisionWithP1;
		

	};
}
#endif