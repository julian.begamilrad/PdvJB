#ifndef BALL_H
#define BALL_H

#include <iostream>
#include <stdlib.h>
#include <time.h>
#include "raylib.h"
#include "PowerUp.h"

using namespace std;

namespace PONGJB
{
	const float SPEEDMULTIPLIER = 1.1f;
	const float LONGANGLESPEED = 0.06f;
	const float SHORTANGLESPEED = 0.01f;
	const int BALLSPEED = 5;
	const int RADIUS = 12;
	class Ball
	{
	public:
		Ball();
		~Ball();
		void init();
		void drawMe();
		void move();
		int testLimits();
		void checkColisionWihtPlayers(Rectangle Player1, Rectangle Player2);
		int checkColisionWihtPowerUps(PowerUp PowUP);
		
		Vector2 getSpeed();
		int getRadius();
		Vector2 ballGetPosition();
		bool getColisionWithP1();
		bool colisionWithP1;
		void setSpeed(Vector2 nSpeed);

	private:

		
		Vector2 position;
		int radius;
		Color myColor;
		Vector2 speed;
		
		void setPosition(Vector2 newPos);

	};
}
#endif